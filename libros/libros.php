<?php

require_once 'librerias/utilidades.inc';
require_once 'librerias/conexion.inc';

$conexion = conectar('libros');

$registros = consultaArray($conexion, "SELECT * FROM libros");

$titulo = "Listado de libros";
$encabezado = "Listado de libros";
$contenido = gridView($registros);

require_once "plantilla.php";
