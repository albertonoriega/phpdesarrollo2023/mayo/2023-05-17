<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?= $titulo ?></title>
    <link rel="stylesheet" href="css/estilos.css">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</head>

<body>
    <?php
    require_once '_menu.php';

    ?>
    <h2><?= $encabezado ?></h2>
    <div class="mostrarDatos">

        <?= $contenido ?>

    </div>
</body>

</html>