<div class="col-lg-8 row mx-auto">
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Nombre</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["nombre"] ?></p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card bg-light">
            <div class="card-header">Apellidos</div>
            <div class="card-body">
                <p class="card-text"><?= $_GET["apellidos"] ?></p>
            </div>
        </div>
    </div>
</div>
