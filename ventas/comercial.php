<?php

require_once 'librerias/utilidades.inc';
require_once 'librerias/conexion.inc';

$conexion = conectar('ventas');

$registros = consultaArray($conexion, "SELECT * FROM comercial");

$titulo = "Listado de comerciales";
$encabezado = "Listado de comerciales";
$contenido = gridView($registros);

require_once "plantilla.php";
