<nav>
    <?php

    $menu =
        dibujarMenu(
            obtenerPagina(),
            [
                "Home" => "index.php",
                "Pedidos" => "pedidos.php",
                "Clientes" => "clientes.php",
                "Comercial" => "comercial.php",
            ],
        );

    ?>
</nav>