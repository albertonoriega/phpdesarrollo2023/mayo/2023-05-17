<nav>
    <?php

    $menu =
        dibujarMenu(
            obtenerPagina(),
            [
                "Home" => "index.php",
                "Libros" => "libros.php",
                "Autores" => "autores.php",
            ],
        );

    ?>
</nav>