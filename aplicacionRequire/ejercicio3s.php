<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php require "./inc/css.inc"; ?>
        <title>Ejercicio 3 suma</title>
    </head>
    <body>
        <?php require "./inc/menu.inc"; ?>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio3s"])) {
                $numeros = $_GET["numeros"];
                $suma = 0;
                $resto = explode(";", $numeros[2]);
                unset($numeros[2]);
                $numeros = array_merge($numeros, $resto);
                foreach ($numeros as $numero) {
                    $suma += $numero;
                }
                require "./inc/resultados3s.inc";
            } else {
                require "./inc/formulario3s.inc";
            }
            ?>
        </div>
        <?php
        require "./inc/js.inc";
        ?>

    </body>
</html>