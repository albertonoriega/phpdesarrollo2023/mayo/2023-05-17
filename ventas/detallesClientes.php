<?php

require_once 'librerias/utilidades.inc';
require_once 'librerias/conexion.inc';

$conexion = conectar('ventas');
// Si se ha pulsado el botón buscar y el inpput no se ha dejado en blanco
if (isset($_GET['buscar']) && !empty($_GET['dato'])) {
    $dato = $_GET['dato'];
    // trim => quitar las comillas
    $seleccion = trim($_GET['consulta'], '"');

    // id y categoaría son números, lo pasamos a int
    if ($seleccion == 'id' || $seleccion == 'categoría') {
        $dato = intval($dato);
    } else  if ($seleccion == 'nombre' || $seleccion == 'apellido1' || $seleccion == 'ciudad') {
        $dato = trim($dato, '"');
        // Si no hay selección vuelve a cargar de nuevo la página
    } else {
        header("Location: clientes.php");
        exit();
    }
} else {
    // Si no se ha pulsado el botón o el input está vacío
    header("Location: clientes.php");
    exit();
}

$conexion = conectar('ventas');

$registros = consultaArray($conexion, "SELECT * FROM cliente WHERE {$seleccion}='{$dato}'");
if (empty($registros)) {
    $contenido = "<h3 style='margin-top: 20px'> No se han encontrado resultados </h3>";
} else {
    $contenido = gridView($registros);
}

$titulo = "Detalle de clientes";
$encabezado = "Cliente con {$seleccion} = {$dato}";


require_once "plantilla.php";

?>


<a href="clientes.php" class="volverClientes">Volver a clientes</a>