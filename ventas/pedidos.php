<?php

require_once 'librerias/utilidades.inc';
require_once 'librerias/conexion.inc';

$conexion = conectar('ventas');

$registros = consultaArray($conexion, "SELECT * FROM pedido");

$titulo = "Listado de pedidos";
$encabezado = "Listado de pedidos";
$contenido = gridView($registros);

require_once "plantilla.php";
