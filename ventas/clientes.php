<?php

require_once 'librerias/utilidades.inc';
require_once 'librerias/conexion.inc';

$conexion = conectar('ventas');

$registros = consultaArray($conexion, "SELECT * FROM cliente");

$titulo = "Listado de clientes";
$encabezado = "Listado de clientes";
$contenido = gridView($registros);

require_once "plantilla.php";


?>


<form action="detallesClientes.php" class="detallesClientes">
    <div>
        <select name="consulta" id="consulta">
            <option value="">Como quieres filtrar</option>
            <option value="id">ID</option>
            <option value="nombre">Nombre</option>
            <option value="apellido1">Primer apellido</option>
            <option value="ciudad">Ciudad</option>
            <option value="categoría">Categoría</option>
        </select>
    </div>
    <div>
        <input type="text" name="dato">
    </div>
    <button type="submit" class="botonBuscar" name="buscar">Buscar <ion-icon name="search-circle-outline"></ion-icon> </ion-icon></button>
</form>