<?php
function obtenerPagina()
{
    // Comprobamos en que página estamos para mostrar activo el li correspondiente del menu 

    // Constantes de servidor
    // __FILE__ nombre del archivo php con la ruta
    //var_dump(__FILE__); //ruta absoluta (desde la raiz del ordenador) y nombre
    // 'C:\laragon\www\Desarrollo2023\php\ejemplos\mayo\2023-05-11\_menu.php'

    // OTRA FORMA

    // variables del servidor
    // $_SERVER es un array con datos del servidor
    // var_dump($_SERVER['PHP_SELF']); // ruta relativa (desde la raiz del proyecto) y nombre
    // '/Desarrollo2023/php/ejemplos/mayo/2023-05-11/index.php'

    // COMO SABER EN QUE PÁGINA ESTAMOS

    // Nos quedamos con la ruta que viene después de la última /
    // $pagina = (strrchr($_SERVER['PHP_SELF'], '/'));

    // OTRA FORMA 
    // Explode crea una array de strings utilizando un carácter
    $a = explode('/', $_SERVER['PHP_SELF']);
    // array_pop te quita el ultimo elemento del array
    $pagina = array_pop($a);
    return $pagina;
}

function dibujarMenu($pagina, $menu)
{
?>
    <ul>
        <?php
        // Crear el menu
        foreach ($menu as $etiqueta => $href) {
            if ($href == $pagina) {
                echo "<li ><a class= 'activo' href=\"{$href}\">{$etiqueta}</a></li>";
            } else {
                echo "<li><a href=\"{$href}\">{$etiqueta}</a></li>";
            }
        }

        ?>
    </ul>
<?php
}


function gridView($registros, $campos = [])
{
    // Si no le pasas los campos a mostrar, pasamos un array vacio

    // Si el array está vacio
    if (empty($campos)) {
        //array_keys => te devuelve todos los indices de un array
        $campos = array_keys($registros[0]); // asignas todos los campos a la variable $campos
    }

    // Con ob_start => acumula todo el codigo sin imprimirlo
    ob_start();
?>
    <div class="contenidoTablas">
        <table>
            <thead>
                <tr>
                    <?php
                    foreach ($campos as $titulo) {
                        echo "<td>{$titulo}</td>";
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($registros as $registro) {
                ?>
                    <tr>
                        <?php
                        foreach ($campos as $titulo) {
                            echo "<td>{$registro[$titulo]}</td>";
                        }
                        ?>
                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
<?php
    // ob_get_clean => Todo lo que se ha guardado, lo devolvemos
    return ob_get_clean();
}
